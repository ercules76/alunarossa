#ifndef RISTORANTE_H
#define RISTORANTE_H

// Definizione grandezza buffer
#define MAX_NAME_LEN 50
#define MAX_NUM_ORDINI 50
#define MAX_TABLE 20
#define ALFA_LEN 3 // Grandezza massima del codice alfanumerico per la gestione dei tavoli 

// Voci del menù (programma)
#define ESCI 0
#define NUOVO_PIATTO_VINO 1
#define NUOVO_TAVOLO 2
#define NUOVO_ORDINE 3
#define CALCOLA_CONTO_TAVOLO 4

// Codici d' errore
#define OK 0
#define TABLE_FOUND 1
#define ITEM_FOUND 2
#define TABLE_NOT_FOUND -1
#define FULL_ARCHIVIE -2
#define ITEM_NOT_FOUND -3


// definizione piatti
typedef struct {
    char nome[MAX_NAME_LEN]; // Nome del piatto
    char tipo[MAX_NAME_LEN]; // Tipo di pietanza
    float prezzo; // Prezzo della portata
    int id; // numero identificativo del piatto, generato automaticamente
}piatto;

// definizione vini
typedef struct {
    char nome[MAX_NAME_LEN]; // Nome del vino
    char tipo[MAX_NAME_LEN]; // Tipo di vino
    float prezzo; // Prezzo della portata
    int annata; // annata del vino
    int id; // numero identificativo del vino, generato automaticamente
}vino;

// struttura per il menù
typedef struct{
    vino archivio_vini[50];
    piatto archivio_piatti[50];
    int size; // contatore progressivo voci del menù
    int size_vini;
    int size_piatti;
}menu;

// Struttura per creazione tavolo
typedef struct{
    char tavolo[ALFA_LEN];
    int numero_di_commensali;
}tavolo;

// Struttura per gestire il gruppo di commensali
typedef struct{
    tavolo archivio_tavoli[MAX_TABLE];
    int size;
}commensali;

typedef struct{
    int itemID; // ID del prodotto da memorizzare nell'ordine
    int quantita;
    char tavolo[ALFA_LEN];
}ordine;

typedef struct{
    ordine ordini[MAX_NUM_ORDINI];
    int size;
}archivio_ordini;

// Funzioni di inizializzazione
void init_menu(menu*);
void init_commensali(commensali*);
void init_ordini(archivio_ordini*);

// Funzioni per popolare il menù 
vino inserisci_nuovo_vino();
piatto inserisci_nuovo_piatto();
void inserisci_ordine_in_archivio(archivio_ordini*, char[], int, int);
             
int inserisci_vino_in_menu(vino, menu*);
int inserisci_piatto_in_menu(piatto, menu*);

// funzioni per la gestione dei commensali
tavolo inserisci_nuovo_tavolo();
int inserisci_gruppo_in_archivio(tavolo, commensali*);

// Funzioni di ricerca
int cerca_esistenza_tavolo(char[], commensali);
int cerca_esistenza_piatto_vino(int, menu);
int cerca_piatto(int, menu);
int cerca_vino(int, menu);
char* cerca_item_da_id(int, menu);
int cerca_numero_commensali_per_tavolo(commensali, char[]);
float cerca_prezzo_prodotto(int, menu);

// Funzioni commerciali
void stampa_conto(char[], archivio_ordini, menu, commensali);

#endif
/* Il ristorante "A luna rossa" ha necessita' di informatizzare le operazioni. Nello specifico il programma dovra' fornire all'utente le seguenti funzionalita':
 1) NUOVO PIATTO O VINO DEL MENU': il programma chiede all'utente se si desidera inserire in menu' un piatto o un vino. In base alla scelta effettuata il programma 
 chiede le seguenti informazioni: per un piatto un nome (es. "Spaghetti allo scoglio"), un tipo di portata (es. "primo piatto") ed un prezzo (es 14$); per un vino, invece, 
 un nome (es "Fiano"), un tipo (es. "Bianco"), un prezzo (es. 14$) ed una annata (es. 2010). Ogni voce del menu' e' caratterizzata da un codice numerico (progressivo) che 
 identifica univocamente un piatto o un vino. Il codice e' generato automaticamente dal programma.
 2) NUOVO TAVOLO DI COMMENSALI: il programma deve consentire la creazione di un nuovo gruppo di commensali, specificando il tavolo (un identificativo alfanumerico) 
 a cui sono seduti e il numero di commensali.
 3) NUOVO ORDINE: il programma deve consentire di aggiungere un piatto/vino alle ordinazioni di un dato tavolo. A tal fine il programma prende in input il codice del tavolo , 
 il codice del piatto/vino e una quantità; verifica la correttezza delle informazioni; stampa a video il nome del piatto/vino e chiede conferma all'utente dell'operazione 
 da effettuare.
 4) CALCOLA CONTO TAVOLO: il programma  prende in input il codice del tavolo e stampa a video la ricevuta con l'elenco delle ordinazioni, la quantita', il relativo costo e il totale.
 5) VISUALIZZA MENU': il programma stampa a video il menu' completo, visualizzando prima i piatti e poi i vini.
 */

#include <stdio.h>
#include <stdlib.h>
#include "ristorante.h"

int main(int argc, char* argv[]) {
    
    int scelta, risultato, i;
    int item, quantita;
    char scelta_menu, check_tavolo[ALFA_LEN], choice;
    piatto new_piatto;
    vino new_vino;
    tavolo new_tavolo;
    
    // Archivi menù e Gruppo commensali
    menu menu;
    commensali commensali;
    archivio_ordini ordini;

    init_menu(&menu);
    init_commensali(&commensali);
    init_ordini(&ordini);

    do {
        printf("\n********** A' LUNA ROSSA **********\n\n");
        printf("\tMenù: \n");
        printf("[1]. INSERIMENTO NUOVO PIATTO O VINO NEL MENU'. \n");
        printf("[2]. NUOVO TAVOLO DI COMMENSALI.\n");
        printf("[3]. NUOVO ORDINE.\n");
        printf("[4]. CALCOLA CONTO TAVOLO.\n");
        printf("[0]. ESCI. \n");
        printf("\tScelta:\n");
        scanf("%d",&scelta);
        switch(scelta) {
        case NUOVO_PIATTO_VINO:
        system("clear");
        do {
            printf("Si vuole inserire un vino [v] oppure un piatto [p]?: ");
            while(getchar() != '\n');
            scanf("%c", &scelta_menu);
            if (scelta_menu != 'v' && scelta_menu != 'V' && scelta_menu != 'p' && scelta_menu != 'P') 
                printf("Scelta non corretta, digitare [v] per vino o [p] per piatto. \n");
        } while(scelta_menu != 'v' && scelta_menu != 'V' && scelta_menu != 'p' && scelta_menu != 'P') ;
         if (scelta_menu == 'v' || scelta_menu == 'V') {
             new_vino = inserisci_nuovo_vino();
             risultato = inserisci_vino_in_menu(new_vino, &menu);
         }      
        else {
            new_piatto = inserisci_nuovo_piatto();
            risultato = inserisci_piatto_in_menu(new_piatto, &menu);
        }
        if(risultato == 0) {
            printf("Ordine inserito correttamente.\n");
        } else {
            printf("ERRORE nell'inserimento ordine nel menu.\n");
        }
        printf("Premere un tasto per continuare...\n");
        getchar();
        while(getchar() != '\n');
        break;
        case NUOVO_TAVOLO:
            system("clear");
            new_tavolo = inserisci_nuovo_tavolo(commensali);
            risultato = inserisci_gruppo_in_archivio(new_tavolo, &commensali);
            if (risultato == 0) 
                printf("Tavolo inserito correttamente in archivio. \n");
            else 
                printf("ERRORE - Raggiunto numero massimo di tavoli.\n");
            printf("Premere un tasto per continuare...\n");
            getchar();
            while(getchar() != '\n');
        break;
        case NUOVO_ORDINE:
            system("clear");
            printf("Inserire codice tavolo: ");
            scanf("%s", check_tavolo);
            risultato = cerca_esistenza_tavolo(check_tavolo, commensali);
            if( risultato == TABLE_FOUND) { 
                printf("Inserire codice del piatto/vino scelto: ");
                scanf("%d", &item);
                risultato = cerca_esistenza_piatto_vino(item, menu);
                    if(risultato >= 0) {
                        printf("Inserire quantità: ");
                        scanf("%d", &quantita);
                        system("clear");
                        printf("Item scelto [%s]: proseguire con l'ordine [S] = si [N] = no?: ", cerca_item_da_id(item, menu));
                        scanf("%s", choice);
                        if(choice == 's' || choice == 'S')
                            inserisci_ordine_in_archivio(&ordini, check_tavolo, item, quantita);
                    } else {
                        printf("Piatto/Vino non trovato");
                    }
            } else {
                printf("Non esiste nessun tavolo con questo codice [%s]\n", check_tavolo);
            }
        break;
        case CALCOLA_CONTO_TAVOLO:
            system("clear");
            printf("Inserire numero del tavolo: ");
            scanf("%s", check_tavolo);
            //stampa_conto(check_tavolo, ordini, menu, commensali);
        break;
        case 5:
            /*
            printf("\tVINI\n");
            for(i = 0; i < menu.size_vini; i++) {
                printf("Nome Vino: %s", menu.archivio_vini[i].nome);
                printf("Tipo Vino: %s", menu.archivio_vini[i].tipo);
                printf("Annata: %d\n", menu.archivio_vini[i].annata);
                printf("Prezzo: %.2f\n", menu.archivio_vini[i].prezzo);
                printf("Codice: %d\n", menu.archivio_vini[i].id);
            }
             printf("\tPIATTI\n");
            for(i = 0; i < menu.size_piatti; i++) {
                printf("Nome Piatto: %s", menu.archivio_piatti[i].nome);
                printf("Tipo Piatto: %s", menu.archivio_piatti[i].tipo);
                printf("Prezzo: %.2f\n", menu.archivio_piatti[i].prezzo);
                printf("Codice: %d\n", menu.archivio_piatti[i].id);
            }*/
            for(i = 0; i < commensali.size; i++) {
                printf("Codice tavolo %s\n", commensali.archivio_tavoli[i].tavolo);
                printf("Numero di commensali a tavolo %d\n", commensali.archivio_tavoli[i].numero_di_commensali);
            }
            break;
            default:
            break;
        }
    }while(scelta != 0);
    return 0;
}
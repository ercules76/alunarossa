#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ristorante.h"

void init_menu(menu *menu) {
    menu->size = 0;
    menu->size_piatti = 0;
    menu->size_vini = 0;
}

void init_commensali(commensali *commensali) {
    commensali->size = 0;
}

void init_ordini(archivio_ordini *ordini) {
    ordini->size = 0;
}

vino inserisci_nuovo_vino() {
    vino new_vino;
    printf("Inserire nome del vino. ");
    while(getchar() != '\n');
    fgets(new_vino.nome, MAX_NAME_LEN, stdin);
    printf("Inserire tipo di vino. ");
    fgets(new_vino.tipo, MAX_NAME_LEN, stdin);
    printf("Inserire annata. ");
    scanf("%d", &new_vino.annata);
    //while(getchar() != '\n');
    printf("Inserire prezzo del vino.");
    scanf("%f", &new_vino.prezzo);
    return new_vino;
}

piatto inserisci_nuovo_piatto() {
    piatto new_piatto;
    printf("Inserire nome del piatto. ");
    while(getchar() != '\n');
    fgets(new_piatto.nome, MAX_NAME_LEN, stdin);
    printf("Inserire tipo di piatto. ");
    fgets(new_piatto.tipo, MAX_NAME_LEN, stdin);
    printf("Inserire prezzo del piatto. ");
    scanf("%f", &new_piatto.prezzo);
    return new_piatto;
}

// Le due funzioni di inserimento prodotti nel menù avranno un id creato in automatico prendendo il progressivo del numero di prodotti totali
// il codice dei vini partirà da 0, mentre quello dei piatti da 20.
// Si è realizzata questa soluzione in quanto solitamente nei menu, a meno che di avere due menu separati quindi uno per i vini e uno per i piatti,
// la lista è suddivisa in categoria e non sono mischiati tra di oro i prodotti.

int inserisci_vino_in_menu(vino new_vino, menu *new_menu) {
    int index = new_menu->size_vini;
    if(new_menu->size < MAX_NUM_ORDINI) {
        strcpy(new_menu->archivio_vini[index].nome, new_vino.nome);
        strcpy(new_menu->archivio_vini[index].tipo, new_vino.tipo);
        new_menu->archivio_vini[index].annata = new_vino.annata;
        new_menu->archivio_vini[index].prezzo = new_vino.prezzo;
        new_menu->archivio_vini[index].id = index;
        new_menu->size_vini++;
        new_menu->size++;
        return OK;
    }
    return FULL_ARCHIVIE;
}

int inserisci_piatto_in_menu(piatto new_piatto, menu *new_menu) {
    int index = new_menu->size_piatti;
    if(new_menu->size < MAX_NUM_ORDINI) {
        strcpy(new_menu->archivio_piatti[index].nome, new_piatto.nome);
        strcpy(new_menu->archivio_piatti[index].tipo, new_piatto.tipo);
        new_menu->archivio_piatti[index].prezzo = new_piatto.prezzo;

        // per evitare di avere un codice del prodotto simile tra piatti e vini, la lista dei piatti
        // nel menù partirà con codice da 20 in su
        new_menu->archivio_piatti[index].id = index + 20;
        new_menu->size_piatti++;
        new_menu->size++;
        return OK;
    }
    return FULL_ARCHIVIE;
}

tavolo inserisci_nuovo_tavolo(commensali _commensali){
    tavolo new_tavolo;
    int risultato;
    do {
    printf("Inserire codice tavolo. ");
    scanf("%s", new_tavolo.tavolo);
    // Check per verificare che il numero massimo del codice tavolo sia di 3 caratteri
    if(strlen(new_tavolo.tavolo) > 3 )
        printf("Il codice del tavolo non è corretto, deve contenere al più 3 caratteri.\n");
    risultato = cerca_esistenza_tavolo(new_tavolo.tavolo, _commensali);
    if (risultato == TABLE_FOUND)
        printf("Un tavolo con questo codice è già presente in archivio, sceglierene uno nuovo.\n");
    } while((strlen(new_tavolo.tavolo) > 3) || (risultato != 0));

    printf("Quanti commensali ci sono? ");
    scanf("%d", &new_tavolo.numero_di_commensali);
    return new_tavolo;
}

int inserisci_gruppo_in_archivio(tavolo _tavolo, commensali *_commensali){
    int index = _commensali->size;
    if (index < MAX_TABLE) {
        strcpy(_commensali->archivio_tavoli[index].tavolo, _tavolo.tavolo);
        _commensali->archivio_tavoli[index].numero_di_commensali = _tavolo.numero_di_commensali;
        _commensali->size++;
        return OK;
    }
    return FULL_ARCHIVIE;
}

int cerca_esistenza_tavolo(char _tavolo[], commensali _commensali) {
    int i;
    int result;
    
    if(_commensali.size == 0)
        return OK;

    for(i = 0; i < _commensali.size; i++) {
        result = strcmp(_tavolo, _commensali.archivio_tavoli[i].tavolo);
        if (result == 0)
            return TABLE_FOUND;
    }
    return TABLE_NOT_FOUND;
}

int cerca_esistenza_piatto_vino(int _item, menu _menu) {
    int i, check;
    char s; // assegnamo il carattere 'P' per i piatti e 'v' per i vini
    int result;
    
    if(_menu.size == 0)
        return ITEM_NOT_FOUND;

    if(_item >= 20) 
    // Siamo in presenza di un piatto
        result = cerca_piatto(_item, _menu);
    else
        result = cerca_vino(_item, _menu);

    return result;
}

char* cerca_item_da_id(int itemId, menu _menu) {
    int i,p;
    int size;
    char *str;
    
    if (itemId >= 20) {
        size = _menu.size_piatti;
        for(i = 0; i < size; i++)
            if(_menu.archivio_piatti[i].id == itemId) {
                str = _menu.archivio_piatti[i].nome;
                p = strlen(_menu.archivio_piatti[i].nome);
                str[p-1] = '\0';
                return str;
            }
    } else {
        size = _menu.size_vini;
        for(i = 0; i < size; i++)
            if(_menu.archivio_vini[i].id == itemId) {
                str = _menu.archivio_vini[i].nome;
                p = strlen(_menu.archivio_vini[i].nome);
                str[p-1] = '\0';
                return str;
            }
    }
}

int cerca_piatto(int item, menu _menu) {
    int i, itr;
    itr = _menu.size_piatti;

    for(i = 0; i < itr; i++) {
        if(_menu.archivio_piatti[i].id == item )
            return item;
    }
    return ITEM_NOT_FOUND;
}

int cerca_vino(int item, menu _menu) {
    int i, itr;
    itr = _menu.size_vini;

    for(i = 0; i < itr; i++) {
        if(_menu.archivio_vini[i].id == item )
            return item;
    }
    return ITEM_NOT_FOUND;
}

void inserisci_ordine_in_archivio(archivio_ordini *ordini, char tavolo[], int item, int quantita) {
    int size;
    size = ordini->size;
    ordini->ordini[size].itemID = item;
    ordini->ordini[size].quantita = quantita;
    strcpy(ordini->ordini[size].tavolo, tavolo);
    ordini->size++;
    printf("Ordine inserito in menù.\n");
}

void stampa_conto(char tavolo[], archivio_ordini ordini, menu _menu, commensali _commensali) {
        // recuperare informazioni del tavolo
    int i,result;
    // ricerca numero di commensali
    int numero_di_commensali;
    // item
    int itemId;
    char *item_name;
    //quantità
    int quantita;
    float prezzo_singolo_prodotto;
    float totale;

    numero_di_commensali = cerca_numero_commensali_per_tavolo(_commensali, tavolo);
    printf("Prodotto\tQ.ta\tCosto");
    for(i = 0; i < ordini.size; i++) {
        if(strcmp(ordini.ordini[i].tavolo, tavolo) == 0) {
            // recupero id e quantità
            itemId = ordini.ordini[i].itemID;
            quantita = ordini.ordini[i].quantita;
            item_name = cerca_item_da_id(itemId, _menu);
            prezzo_singolo_prodotto = cerca_prezzo_prodotto(itemId, _menu);
            printf("%s\t%d\t%.2f", item_name, quantita, prezzo_singolo_prodotto);
            totale += prezzo_singolo_prodotto;
        }
    }
    totale *= numero_di_commensali;
    printf("Totale: %.2f €", totale);
}

int cerca_numero_commensali_per_tavolo(commensali _commensali, char tavolo[]) {
    int size, i, result;
    size = _commensali.size;
    for (i = 0; i < size; i++) {
        if(strcmp(_commensali.archivio_tavoli[i].tavolo, tavolo) == 0)
            return _commensali.archivio_tavoli[i].numero_di_commensali;
    }
    return TABLE_NOT_FOUND;
}

float cerca_prezzo_prodotto(int itemId, menu _menu) {
    int size = _menu.size;
    int i;

    if(size == 0)
        return ITEM_NOT_FOUND;
    
    if(itemId < 20) {
        size = _menu.size_vini;
        for(i = 0; i < size; i++)
            if(_menu.archivio_vini[i].id == itemId)
                return _menu.archivio_vini[i].prezzo;
    } else {
        size = _menu.size_piatti;
        for(i = 0; i < size; i++)
            if(_menu.archivio_piatti[i].id == itemId)
                return _menu.archivio_piatti[i].prezzo;
    }
}